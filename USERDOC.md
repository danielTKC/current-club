# Current Club

## Team Members

- Josh C Keller
- Sam R Ramos 
- Katie N Cooper
- Daniel I Terreros

# Homepage Overview

Welcome to The Current Club, your ultimate destination for booking suites and tickets for exciting events at our stadium! Whether you're looking to enjoy the whole season from the comfort of a luxury suite or just need a single match ticket, we've got you covered.

# Navigation Options

1. **Login/Register**:
   - If you're a returning user, simply log in to access your account and manage your bookings.
   - New to The Current Club? Join the club by registering for an account to unlock exclusive offers and personalized experiences.

2. **Navigation Options**:
   - **Season Tickets**: Experience the thrill of every match throughout the season with our season ticket packages.
   - **Single Match Tickets**: Can't commit to the whole season? No problem! Purchase tickets for individual matches and join the excitement.
   - **Stadium Parking**: Secure your parking spot hassle-free for a convenient game day experience.
   - **Luxury Suites**: Elevate your game day experience with our luxurious suite options, perfect for entertaining clients, friends, or family.

![Homepage](src/media/photos/markdown/index.png "Homepage")

## How to Use The Current Club

1. **Logging In/Registering**:
   - Click on the "Login" button at the top right corner of the homepage.
   ![Login Page](src/media/photos/markdown/login.png "Login Page")
   - Enter your credentials if you're an existing user. If not, click on "Join The Club" and follow the prompts to create an account.

2. **Exploring Ticket Options**:
   ![Matches Page](src/media/photos/markdown/matches.png "Matches Page")
   - From the homepage, click on the desired navigation option:
     - **Single Match Tickets**: Browse upcoming matches and choose the one you'd like to attend. Clicking on a match will take you to the suites page.
     - **Luxury Suites**: Discover our luxurious suite options and select the one that meets your needs. Clicking on a suite will take you to the matches page for that suite.

3. **Booking Process**:
   ![Suites Page](src/media/photos/markdown/suites.png "Suites Page")
   - After selecting your desired option, follow the prompts to customize your booking:
     - Suite Reservation or match day option.
     - Select additional amenities or add-ons if available.
     - Review your booking details, including date, time, and location.
     - Proceed to checkout, where you can review your order summary and make any necessary adjustments.
     - Complete the payment process securely to confirm your booking.

4. **Payment Process**:
   ![Change Reservations](src/media/photos/markdown/changeres.png "Change Reservations")
   - Upon confirming your booking, you will be directed to the payment page.
   - Enter your payment information securely, including credit/debit card details.
   - Review the payment summary and total amount before finalizing the transaction.
   - Once payment is processed successfully, you can access all reservations in your account.

5. **Managing Bookings**:
   - Logged-in users can access their bookings and account settings from the dashboard.
   - View upcoming bookings, edit details, or cancel if necessary.
