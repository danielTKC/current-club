document.addEventListener("DOMContentLoaded", function () {
    fetchAllMatches()
});

function fetchAllMatches() {
    fetch(`/api/matches/`)
        .then(response => response.json())
        .catch(error => console.error('Error:', error));
}

function viewOptions(matchJson) {
    // Parse the match JSON string to an object
    const match = JSON.parse(matchJson);

    // Populate the modal with the match information
    document.getElementById('matchImage').src = match.match_image;
    document.getElementById('suiteName').textContent = match.opponent;
    // Populate other elements with match data as needed

    // Show the modal
    const modal = new bootstrap.Modal(document.getElementById('suiteModal'));
    modal.show();
}

document.getElementById('cancelButton').addEventListener('click', () => {
    const modal = bootstrap.Modal.getInstance(document.getElementById('suiteModal'));
    modal.hide();
});

document.getElementById('saveButton').addEventListener('click', () => {
    // Add your logic for saving the changes here
    const modal = bootstrap.Modal.getInstance(document.getElementById('suiteModal'));
    modal.hide();
});