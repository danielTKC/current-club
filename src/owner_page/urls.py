from django.urls import path
from . import views
from .views import match_analytics, suiteAnalytics, updatePrice

urlpatterns = [
    path('', views.owner_page, name='owner_page'),
    path('matches/<int:match_id>/', match_analytics, name='match_analytics'),
    path('updatePrice/<int:match_id>/', updatePrice, name='updatePrice'),
    path('suiteAnalytics/', suiteAnalytics, name="suiteAnalytics")
    

]
