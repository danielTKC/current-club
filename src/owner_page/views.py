import requests
from suiteselector.helper_methods import ticketsSoldForMatch, formatPrice
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.urls import reverse

from KcCurrent import settings
from restfulclub.models import Match

from restfulclub.models import Reservation, Suite


def owner_page(request):
    if request.user.is_staff :
        context = {}
        response = requests.get('http://127.0.0.1:8000/api/matches/')
        matches = response.json()
        context['matches'] = matches
        return render(request, 'owner_page/owner_page.html', context)
    else :
        return render(request, 'suiteselector/index.html')


def match_analytics(request, match_id):
    user = request.user
    if user.is_staff : 
        token = user.auth_token.key
        headers = {
            'Authorization': f'Token {token}',
        }

        reservations_data = requests.get(f'http://127.0.0.1:8000/api/reservations/', headers=headers).json()
        match_url = f'http://127.0.0.1:8000/api/matches/{match_id}/'
        canChangePrice = 0
        for reservation in reservations_data:
            match = reservation.get('match')
            if match == match_url:
                canChangePrice = 1

        match = requests.get(f'http://127.0.0.1:8000/api/matches/{match_id}/').json()

        # Retrieve all suites
        suites = Suite.objects.all()

        # Check the availability of each suite for the match
        for suite in suites:
            reservation = Reservation.objects.filter(suite=suite, match_id=match_id).first()
            if reservation:
                suite.availability = 'Unavailable'
            else:
                suite.availability = 'Available'

        context = {
            'canChangePrice': canChangePrice,
            'match': match,
            'suites': suites,
        }

        return render(request, 'owner_page/match_analytics.html', context)
    else :
        return render(request, 'suiteselector/index.html')

def updatePrice(request, match_id):
    user = request.user
    # Retrieve the token from the user's authentication information
    token = user.auth_token.key
    headers = {
        'Authorization': f'Token {token}',
    }
    if request.method == "POST" :
        multiplier = request.POST.get('price_multiplier')
        
        patchData = {
            'price_multiplier': multiplier
        }
            
        response = requests.patch(f'http://127.0.0.1:8000/api/matches/{match_id}/', data=patchData, headers=headers)
        if response.status_code == 200: 
            messages.add_message(request, messages.SUCCESS, "SUCCESS: Price Updated.")
        if response.status_code == 400: 
            messages.add_message(request, messages.ERROR, "ERROR: Please try again.")
    return redirect(reverse('match_analytics', args=[match_id]))

def suiteAnalytics(request) :
    if request.user.is_staff:
        suites_data = requests.get('http://127.0.0.1:8000/api/suites/').json()
        suites = []
        for suite_data in suites_data :
                if 'suite_revenue' in suite_data :
                    suite_data['suite_revenue'] = formatPrice(suite_data['suite_revenue'])
                suites.append(suite_data)
        context = {
            'suites': suites
        }
        
        return render(request, 'owner_page/suite_analytics.html', context)
    else :
        return render(request, 'suiteselector/index.html')