document.addEventListener("DOMContentLoaded", function () {
    const urlParams = new URLSearchParams(window.location.search);
    const matchId = urlParams.get('matchId');

    if (matchId) {
        // Fetch and display suites available for the selected match
        fetchAvailableSuitesForMatch(matchId);
    } else {
        // Fetch and display all suites
        fetchAllSuites();
    }
});

function fetchAvailableSuitesForMatch(matchId) {
    fetch(`/api/matches/${matchId}/`)
        .then(response => response.json())
        .then(data => {
            const suites = data.available_suites;
            displaySuites(suites);
        })
        .catch(error => console.error('Error:', error));
}

function fetchAllSuites() {
    fetch(`/api/suites/`)
        .then(response => response.json())
        .then(suites => {
            displaySuites(suites);
        })
        .catch(error => console.error('Error:', error));
}

function displaySuites(suites) {
    console.log("We're displaying suites")
}


function viewOptions(suiteId) {
    window.location.href = `/matches/?suiteId=${suiteId}`;
}

function bookNow(matchId, suiteId) {
    console.log("Booked");
    console.log(`${matchId}`);
    console.log(`${suiteId}`);
    fetch('/api/check-auth/')
    .then(response  => {
        console.log(response)
        if (response.ok) {
            window.location.href = `/reservationForm/${matchId}/${suiteId}/`;
        }
        else {
            window.location.href = `/login/?next=/reservationForm/${matchId}/${suiteId}/`;
        }
    });
}function highlightSuite(suiteName) {
    console.log("Highlighting suite:", suiteName);

    // Remove highlight from all suites
    document.querySelectorAll('.suite-container').forEach(suite => {
        console.log("Removing highlighted-suite class from:", suite.id);
        suite.classList.remove('highlighted-suite');
    });

    // Add highlight to the selected suite
    const selectedSuite = document.getElementById(suiteName);
    if (selectedSuite) {
        console.log("Adding highlighted-suite class to:", selectedSuite.id);
        selectedSuite.classList.add('highlighted-suite');
    }
}


