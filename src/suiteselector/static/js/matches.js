/*!
* Author: Daniel Terreros
* This is the script that pulls down available matches, checks to see if user is logged in if they try to book
* Sends an alert and sends them to the login page.
*/

document.addEventListener("DOMContentLoaded", function () {
    const urlParams = new URLSearchParams(window.location.search);
    const suiteId = urlParams.get('suiteId');

    if (suiteId) {
        // Fetch and display suites available for the selected match
        fetchAvailableMatchesForSuite(suiteId);
    } else {
        // Fetch and display all suites
        fetchAllMatches();
    }
});

function fetchAvailableMatchesForSuite(suiteId) {
    fetch(`/api/suites/${suiteId}/`)
        .then(response => response.json())
        .then(data => {
            const matches = data.available_matches;
            displayMatches(matches);
        })
        .catch(error => console.error('Error:', error));
}

function fetchAllMatches() {
    fetch(`/api/matches/`)
        .then(response => response.json())
        .catch(error => console.error('Error:', error));
}



function bookNow(matchId, suiteId){
    fetch('/api/check-auth/')
    .then(response  => {
        console.log(response)
        if (response.ok) {
            window.location.href = `/reservationForm/${matchId}/${suiteId}/`;
        }
        else {
            window.location.href = `/login/?next=/reservationForm/${matchId}/${suiteId}/`;
        }
    });
}

//console.log('matches.js loaded');
function viewOptions(matchId) {
    window.location.href = `/suites/?matchId=${matchId}`;
  }