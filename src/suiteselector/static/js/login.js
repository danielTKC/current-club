document.getElementById('loginForm').addEventListener('submit', function (e) {
    e.preventDefault();
    document.getElementById('loginError').style.display = 'none'; // Hide the error message on new submission

    const formData = new FormData(this);
    const data = Object.fromEntries(formData.entries());

    // Capture "next" parameter from the URL
    const urlParams = new URLSearchParams(window.location.search);
    console.log('Search params:', window.location.search);

    const nextUrl = urlParams.get('next'); // This will be null if "next" is not in the URL
    console.log('url', nextUrl);

    const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    fetch('/api/login/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken,
        },
        body: JSON.stringify({username: data.username, password: data.password}),
    })
    .then(response => {
        if (response.ok) {
            return response.json().then(data => {
                console.log('Login success:', data);
                localStorage.setItem('token', data.token); // Store the token

                // Conditional redirection based on "next" parameter
                if(nextUrl) {
                    window.location.href = nextUrl; // Redirect to the URL specified by "next"
                } else {
                    window.location.href = '/'; // Redirect to the main page if "next" is not specified
                }
            });
        } else {
            // Handle login failure
            return response.json().then(error => {
                console.error('Login failed:', error);
                // Update the UI to show an error message

                document.getElementById('loginError').style.display = 'block';
            });
        }
    })
    .catch(error => {
        console.error('Network error:', error);
        document.getElementById('loginError').innerText = "A network error occurred. Please try again.";
    });
});
