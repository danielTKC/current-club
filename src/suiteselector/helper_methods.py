from datetime import datetime, timedelta
from django.contrib.humanize.templatetags.humanize import intcomma
import re
import requests

def isCancellable(sMatchTime) : 
    matchTime = datetime.fromisoformat(sMatchTime[:-1])
    timeDifference = matchTime - datetime.now()
    timeDelta = timedelta(hours=48)
    if (timeDifference > timeDelta) :
        return True
    else : 
        return False

def cancellationRefund(sMatchTime, price):
    matchTime = datetime.fromisoformat(sMatchTime[:-1])
    timeDifference = matchTime - datetime.now()
    timeDelta = timedelta(days=7)
    #return full refund if cancelled more than 7 days in advance
    if (timeDifference > timeDelta) :
        return price
    #return 75% refund if cancelled within one week
    else : 
        return price * .75
    
def formatPrice(price) :
    return "$%s%s" % (intcomma(int(price)), ("%.02f" % price)[-3:])

def mask_cc_number(ccnum, digits_to_keep=4, mask_char='*'):
    cc_string = str(ccnum)
    cc_string_total = sum(map(str.isdigit, cc_string))

    if digits_to_keep >= cc_string_total:
        return "Not enough numbers. Add 10 or more numbers to the credit card number."

    digits_to_mask = cc_string_total - digits_to_keep
    masked_cc_string = mask_char * digits_to_mask + cc_string[-digits_to_keep:]

    return masked_cc_string

def ticketsSoldForMatch(match_url, reservations_data):
    for reservation in reservations_data:
        # Assuming 'match' key contains the URL of the match
        if reservation.get('match') == match_url:
            return True
    return False

