from django import template

register = template.Library()

@register.filter
def range_from_one(value):
    return range(1, value + 1)