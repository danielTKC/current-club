import requests
from django.shortcuts import render, redirect
from django.template import context
from django.contrib.auth.decorators import login_required
from django.contrib.humanize.templatetags.humanize import intcomma
from .helper_methods import isCancellable, cancellationRefund, formatPrice, mask_cc_number
from django.contrib import messages


def indexPageView(request):
    return render(request, "suiteselector/index.html" )

def matchesPageView(request):
    suite_id = request.GET.get("suiteId")
    context = {}

    if suite_id:
        response = requests.get(f'http://127.0.0.1:8000/api/suites/{suite_id}/')
        data = response.json()
        matches = data.get('available_matches', [])
        total_price = data.get('total_price')
        context['suite_id'] = suite_id
        context['matches'] = []

        for match in matches:
            multiplier = match.get('multiplier')
            price = total_price * multiplier
            match['price_multiplier'] = formatPrice(price)
            context['matches'].append(match) 
    else : 
        response = requests.get('http://127.0.0.1:8000/api/matches/')
        matches = response.json()
    context['matches'] = matches
    return render(request, 'suiteselector/matches.html', context)

def suitesPageView(request) :
    match_id = request.GET.get('matchId')
    context = {}

    if match_id:
        response = requests.get(f'http://127.0.0.1:8000/api/matches/{match_id}/')
        data = response.json()
        multiplier = data.get('multiplier')
        suites_data = data.get('available_suites', [])
        for suite_data in suites_data :
            if 'total_price' in suite_data :
                suite_data['total_price'] = suite_data['total_price'] * multiplier
        context['match_id'] = match_id
    else:
        response = requests.get('http://127.0.0.1:8000/api/suites/')
        suites_data = response.json()
    suites = []
    for suite_data in suites_data :
            if 'total_price' in suite_data :
                suite_data['total_price'] = formatPrice(suite_data['total_price'])
            suites.append(suite_data)
    context['suites'] = suites
    return render(request, 'suiteselector/suites.html', context)


def registrationPageView(request):
    return render(request, "suiteselector/registration.html" )

@login_required
def reservationFormPageView(request, match_id, suite_id) :
    user = request.user
    match = requests.get(f'http://127.0.0.1:8000/api/matches/{match_id}/').json()
    suite = requests.get(f'http://127.0.0.1:8000/api/suites/{suite_id}/').json()
    total_price = suite['total_price'] * match['multiplier']
    months = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    years = {2024, 2025, 2026, 2027, 2028, 2029, 2030}
    months = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    years = {2024, 2025, 2026, 2027, 2028, 2029, 2030}
    context = {
    'user': user,
    'match': match,
    'suite': suite,
    'total_price': formatPrice(total_price),
    'months' : months,
    'years' : years,
}

    if request.method =="POST" :
        first_name = request.POST.get("firstName")
        last_name = request.POST.get("lastName")
        email = request.POST.get("email")
        credit_card = request.POST.get("creditCard")
        ccv = request.POST.get("ccv")
        qty = suite.get('capacity')
        token = request.POST.get("token")

        reservationData = {
            "match" : f'http://127.0.0.1:8000/api/matches/{match_id}/',
            "suite" : f'http://127.0.0.1:8000/api/suites/{suite_id}/',
            "total_price" : str(total_price),
            "credit_card": credit_card,
            "ccv": ccv,
            "qty": qty
        }

        headers = {
            "Authorization" : f'Token {token}',
        }
        
        response = requests.post('http://127.0.0.1:8000/api/reservations/', data=reservationData, headers=headers)
        if response.status_code == 201 :
            
            reservationCount = suite['reservations'] + 1
            revenue = suite['suite_revenue'] + total_price   
            reservationCountData = {
                "reservations": reservationCount,
                "revenue": revenue,
            }
            requests.patch(f'http://127.0.0.1:8000/api/suites/{suite_id}/', data=reservationCountData, headers=headers)
            messages.add_message(request, messages.SUCCESS, "Success: Reservation Created")
            return redirect("displayReservations")
        else :
            messages.add_message(request, messages.ERROR, "Error: Failed to make reservation")
            return render(request, "suiteselector/reservationForm.html", context)

    return render(request, "suiteselector/reservationForm.html", context)

@login_required
def displayReservationsPageView(request):
    user = request.user
    # Retrieve the token from the user's authentication information
    token = user.auth_token.key
    headers = {
        'Authorization': f'Token {token}',
    }
    response = requests.get(f'http://127.0.0.1:8000/api/reservations/?user={user.id}', headers=headers)
    if response.status_code == 200:
        reservations_data = response.json()
        reservations = []

        for reservation_data in reservations_data :
            if 'credit_card' in reservation_data :
                reservation_data['credit_card'] = mask_cc_number(reservation_data['credit_card'])
            if 'total_price' in reservation_data :
                reservation_data['total_price'] = formatPrice(reservation_data['decimal_price'])
            reservations.append(reservation_data)
    else:
        reservations = []
    context = {
        'reservations': reservations,
    }
    return render(request, 'suiteselector/displayReservations.html', context)

@login_required
def editReservationPageView(request, reservation_id, suite_id) :
    user = request.user
    # Retrieve the token from the user's authentication information
    token = user.auth_token.key
    headers = {
        'Authorization': f'Token {token}',
    }
    response = requests.get(f'http://127.0.0.1:8000/api/reservations/{reservation_id}', headers=headers)
    if response.status_code == 200 :
        reservation = response.json()
        newSuite = requests.get(f'http://127.0.0.1:8000/api/suites/{suite_id}/').json()
        originalPrice = reservation.get('decimal_price')
        match = reservation.get('match_details', {})
        multiplier = match.get('multiplier')
        newPrice = newSuite.get('total_price') * multiplier
        refund = formatPrice(originalPrice - newPrice)
        upcharge = formatPrice(newPrice - originalPrice)
        newPrice = formatPrice(newPrice)
        ccNum = mask_cc_number(reservation.get('credit_card'))
        
    else :
        reservation = []
        
    context = {
        'reservation': reservation,
        'suite': newSuite, 
        'refund': refund,
        'upcharge': upcharge,
        'newPrice': newPrice,
        'ccNum': ccNum,
        }
    return render(request, 'suiteselector/editReservation.html', context)

def patchReservationView(request, reservation_id, new_suite_id):
    user = request.user
    token = user.auth_token.key
    headers = {'Authorization': f'Token {token}'}

    reservation_response = requests.get(f'http://127.0.0.1:8000/api/reservations/{reservation_id}/', headers=headers).json()
    match = reservation_response.get('match_details', {})
    multiplier = match.get('multiplier')
    
    suite_response = requests.get(f'http://127.0.0.1:8000/api/suites/{new_suite_id}/').json()
    new_price = suite_response.get('total_price')
    new_price = new_price * multiplier
    # Update reservation with new suite ID and new price
    reservation_data = {
        'suite': f'http://127.0.0.1:8000/api/suites/{new_suite_id}/',
        'total_price': new_price
    }
    
    response = requests.patch(f'http://127.0.0.1:8000/api/reservations/{reservation_id}/', data=reservation_data, headers=headers)

    if response.status_code == 200:
        # Reservation updated successfully
        messages.add_message(request, messages.SUCCESS, "Success: Reservation Updated")
        reservation = suite_response.get('reservations') + 1
        newRevenue = suite_response.get('suite_revenue')
        newRevenue = newRevenue + new_price
        analytic_data = {
            'reservations': reservation,
            'revenue': newRevenue
        }
        requests.patch(f'http://127.0.0.1:8000/api/suites/{new_suite_id}/', data=analytic_data, headers=headers)
        
        oldSuite = reservation_response.get('suite_details', {})
        cancellation = oldSuite.get('cancellations') + 1
        oldRevenue = oldSuite.get('suite_revenue')
        oldPrice = reservation_response.get('decimal_price')
        oldRevenue = oldRevenue - oldPrice
        oldSuiteId = oldSuite.get('id')
        
        cancellation_data = {
            'cancellations': cancellation,
            'revenue': oldRevenue,
        }
        requests.patch(f'http://127.0.0.1:8000/api/suites/{oldSuiteId}/', data=cancellation_data, headers=headers)
        return redirect('displayReservations') #placeholder display confirmation message
    else:
        # Failed to update reservation
        return JsonResponse({'success': False, 'message': 'Failed to update reservation.'}, status=400)

@login_required
def confirmCancellation(request, reservation_id) :
    user = request.user
    token = user.auth_token.key
    headers = {'Authorization': f'Token {token}',}
    reservation = requests.get(f'http://127.0.0.1:8000/api/reservations/{reservation_id}', headers=headers).json()
    match_details = reservation.get('match_details', {})
    matchTime = match_details.get('match_date')
    
    if isCancellable(matchTime) :
        messages.add_message(request, messages.WARNING, "Warning: Your reservation will be deleted. Reservations cancelled within 48 hours of the match, will be charged a 25 percent fee")
        context = {
            'cancelId': reservation_id
        }
        return render(request, 'suiteselector/displayReservations.html', context)
    else :
        messages.add_message(request, messages.INFO, "Reservations cannot be cancelled within 48 hours of the match")
        return redirect('displayReservations')
    
@login_required
def deleteReservationView(request, reservation_id):
    user = request.user
    token = user.auth_token.key
    headers = {'Authorization': f'Token {token}',}
    reservation = requests.get(f'http://127.0.0.1:8000/api/reservations/{reservation_id}', headers=headers).json()
    match_details = reservation.get('match_details', {})
    matchTime = match_details.get('match_date')
    
    
    suite = reservation.get('suite_details', {})
    reservationPrice = reservation.get('decimal_price')
    refundAmount = cancellationRefund(matchTime, reservationPrice)
    suite_id = suite.get('id')
    
    response = requests.delete(f'http://127.0.0.1:8000/api/reservations/{reservation_id}', headers=headers)
        
    if response.status_code == 204:
        cancellations = suite.get('cancellations') + 1
        revenue = suite.get('suite_revenue') - refundAmount
            
        analyticData = {
            'cancellations': cancellations,
            'revenue': revenue
        }
        requests.patch(f'http://127.0.0.1:8000/api/suites/{suite_id}/', data=analyticData, headers=headers)
            
        messages.add_message(request, messages.SUCCESS, "SUCCESS: Reservation Deleted.")            
        return redirect('displayReservations')#placeholder display confirmation message
    else :
        messages.add_message(request, messages.ERROR, "Error: Unable to Cancel Reservation")
        return redirect('displayReservations')#placeholder display error message
     
    
    
    
@login_required
def editSuitePageView(request, reservation_id, match_id) :
    user = request.user
    # Retrieve the token from the user's authentication information
    token = user.auth_token.key
    
    match = requests.get(f'http://127.0.0.1:8000/api/matches/{match_id}/').json()
    matchTime = match.get('match_date')
    multiplier = match.get('multiplier')
    if isCancellable(matchTime) :
        suites = []
        suites_data = match.get('available_suites', [] )
        for suite_data in suites_data :
            if 'total_price' in suite_data :
                suite_data['total_price'] = suite_data['total_price'] * multiplier
                suite_data['total_price'] = formatPrice(suite_data['total_price'])
            suites.append(suite_data)
        
        context = {
            'reservation': reservation_id,
            'suites': suites,
        }
        return render(request, 'suiteselector/suites.html', context)
    else :
        messages.add_message(request, messages.INFO, "Reservations cannot be changed within 48 hours of the match.")
        return redirect('displayReservations')
    