from django.urls import path
from .views import indexPageView, matchesPageView, registrationPageView, suitesPageView, reservationFormPageView, displayReservationsPageView, editReservationPageView, editSuitePageView, patchReservationView, deleteReservationView, confirmCancellation
            
urlpatterns = [
    path("", indexPageView, name="index"),
    path("matches/", matchesPageView, name="matches"), 
    path("registration/", registrationPageView, name="registration"),
    path("suites/", suitesPageView, name="suites"), 
    path("suites/<int:reservation_id>/<int:match_id>", editSuitePageView, name="editSuite"),
    path("reservationForm/<int:match_id>/<int:suite_id>/", reservationFormPageView, name="reservationForm"),
    path("displayReservations/", displayReservationsPageView, name="displayReservations"),
    path("confirmCancellation/<int:reservation_id>", confirmCancellation, name="confirmCancellation"),
    path("editReservation/<int:reservation_id>/<int:suite_id>/", editReservationPageView, name="editReservation"),
    path("patchReservation/<int:reservation_id>/<int:new_suite_id>/", patchReservationView, name="patchReservation"),
    path("deleteReservation/<int:reservation_id>/", deleteReservationView, name="deleteReservation"),
            ]